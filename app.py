import gradio as gr
from PIL import Image

# model load


def predict(inp):
    inp = Image.fromarray(inp.astype('uint8'), 'RGB')

    # preprocess input

    # model predict

    # convert to pil image or np array

    # return the images
    return inp # if you use many outputs `return a, b, ...`


gr.Interface(
    fn=predict,
    inputs=[
        gr.inputs.Image() # you can have many inputs
    ],
    outputs=[
        gr.inputs.Image() # you can have many outputs
    ],
    title="Titolo",
    description="Description of the app",
    examples=[
        "gabbiano.jpg" # if you have many input use a list ["gabbiano.jpg", "other input", ...]
    ]
).launch()
